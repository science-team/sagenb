Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: SageNB
Upstream-Contact: The Sage Development Team <sage-notebook@googlegroups.com>
Source: https://github.com/sagemath/sagenb
Files-Excluded:
 *.min.js
 *.mini.js
 *-min.js
 sagenb/data/codemirror/*
 sagenb/data/highlight/*
 sagenb/data/jquery/jquery*.js
 sagenb/data/jquery/plugins/colorpicker/*
 sagenb/data/jquery/plugins/farbtastic/*
 sagenb/data/jquery/plugins/form/*
 sagenb/data/jquery/plugins/jpicker/*
 sagenb/data/jquery/plugins/jquery.bgiframe.min.js
 sagenb/data/jqueryui/*
 sagenb/data/json/*
 sagenb/data/openid-realselector/demo.html
 sagenb/data/tiny_mce/*
 sagenb/data/sage/css/main.css
 sagenb/data/sage/css/test_report.css
 sagenb/data/sage3d/lib/sage3d.jar
Comment:
 - bgiframe is a IE6-specific work-around, and upstream omits its source anyway
 - openid-realselector/demo.html loads a remote jQuery, just rm it, it's unused
 - everything else is supplied by Debian system packages
 - farbtastic and jpicker aren't necessary, just use libjs-jquery-colorpicker

Files: *
Copyright: 2009-2016 SageNB contributors <sage-notebook@googlegroups.com>
           2006-2016 William Stein <wstein@gmail.com>
License: GPL-3+

Files: sagenb/data/jquery/plugins/achtung/*
Copyright: 2009 Josh Varner (http://achtung-ui.googlecode.com/)
License: Expat

Files: sagenb/data/jquery/plugins/extendedclick/*
Copyright: 2011 Jason Grout <jason-sage@creativetrax.com>
           2008 Minus Creative (http://minuscreative.com)
Comment: Homepage at https://github.com/jasongrout/jquery-extended-click
 Can't find the GPL-LICENSE.txt that it refers to, anywhere online.
 https://web.archive.org/web/20081004032021/http://plugins.jquery.com/node/4158
 does not mention which version either. GPL-3 was released in 2007.
License: Expat or GPL-3+

Files: sagenb/data/jquery/plugins/jeditable/*
Copyright: 2006-2009 Mika Tuupola, Dylan Verheul
Comment: Homepage at http://www.appelsiini.net/projects/jeditable
License: Expat

Files: sagenb/data/openid-realselector/*
Copyright: 2009 Martin Conte Mac Donell <Reflejo@gmail.com>
Comment:
 License from https://code.google.com/archive/p/openid-realselector/
 Copyright from https://code.google.com/archive/p/openid-realselector/source/default/commits
License: BSD-3-Clause

Files: sagenb/data/zorn/wz_jsgraphics.js
Copyright: 2002-2007 Walter Zorn <http://www.walterzorn.de/>
           2002-2007 Thomas Frank
           2002-2007 John Holdsworth
           2002-2007 Matthieu Haller
License: LGPL-2.1+

Files: sagenb/testing/selenium/selenium.py
Copyright: 2006 ThoughtWorks, Inc.
License: Apache-2.0
 On Debian systems, the complete text of the Apache 2.0 License can
 be found in the file "/usr/share/common-licenses/Apache-2.0".

Files: debian/*
Copyright: 2016 Ximin Luo <infinity0@debian.org>
License: GPL-3+

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: BSD-3-Clause
 On Debian systems, the complete text of the BSD 3 Clause License can
 be found in the file "/usr/share/common-licenses/BSD", replacing
 references to the University of California with the appropriate
 copyright holder.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License (LGPL) as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1".

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".
